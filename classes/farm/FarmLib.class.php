<?php
/**
 * Created by PhpStorm.
 * User: amfipolis
 * Date: 29/09/2018
 * Time: 19:45
 */

namespace farm;

/**
 * Class FarmLib
 * @package farm
 */
class FarmLib
{

    /**
     * @param array $species
     * @return bool
     */
    static public function check_for_human ( array $species = array () ): bool
    {
        $answer = false;
        if ( count ( $species ) > 0 ) {
            foreach ( $species as $key => $item ) {
                if ( get_class ( $item ) == 'farm\human' ) {
                    $answer = true;
                    break;
                }
            }
        }
        return $answer;
    }

    /**
     * @param $species
     * @return bool
     */
    static public function is_game_over ( $species )
    {
        // game is over when either no rounds left, no human left, or only a human is left
        return ( ( ! self::check_for_one_each ( $species ) ) || self::get_session_round () == 0 || ( ! self::check_for_human ( $species ) ) || self::check_only_human ( $species ) );
    }


    /**
     * @param array $species
     * @return bool
     */
    static public function check_only_human ( array $species = array () )
    {
        return self::check_for_human ( $species ) && ( count ( $species ) == 1 );
    }


    /**
     * @return int
     */
    static public function get_session_round (): int
    {
        // check for session existence
        $has_session = session_status () == PHP_SESSION_ACTIVE;
        if ( ! $has_session && ! headers_sent () ) {
            session_start ();
        }
        // set default
        $round = 50;
        if ( isset( $_SESSION[ 'round' ] ) ) {
            $round = unserialize ( $_SESSION[ 'round' ] );
        }
        // recover from faulty value state
        if ( $round < 0 ) {
            $round = 50;
        }
        return $round;
    }

    /**
     * decrease session rounds
     */
    static public function decrease_session_rounds ()
    {
        $rounds = self::get_session_round () - 1;
        self::set_session_round ( $rounds );
    }


    /**
     * @param array $species
     */
    static public function show_farm ( array & $species = array () )
    {
        if ( count ( $species ) > 0 ) {
            foreach ( $species as $key => $animal ) {
                if ( ! $animal->is_dead () ) {
                    $animal->show ();
                }
                else {
                    unset( $species[ $key ] );
                }
            }
        }
    }


    /**
     * @param int $round
     */
    static public function set_session_round ( int $round )
    {
        $_SESSION[ 'round' ] = serialize ( $round );
    }


    /**
     * @param array $species
     */
    static public function clear_farm ( array & $species = array () )
    {
        foreach ( $species as $key => $item ) {
            if ( $item->is_dead () ) {
                unset( $species[ $key ] );
            }
        }
        self::set_session_species ( $species );
    }


    /**
     * @param $species
     */
    static public function set_session_species ( $species )
    {
        $_SESSION[ 'species' ] = serialize ( $species );
    }


    /**
     * @param $species
     */
    static public function feed_farm ( & $species )
    {
        // feed the lucky and reduce the unlucky
        $species_keys = array_keys ( $species );
        // find a lucky species
        $lucky = rand ( 0 , count ( $species_keys ) - 1 );

        foreach ( $species as $key => $item ) {
            if ( $key == $lucky ) {
                /** @noinspection PhpUndefinedMethodInspection */
                $species[ $key ]->feed ();
            }
            else {
                /** @noinspection PhpUndefinedMethodInspection */
                $species[ $key ]->decrease ();
            }
        }
        self::clear_farm ( $species );
        self::set_session_species ( $species );
        self::decrease_session_rounds ();
    }


    /**
     * @return array
     */
    static public function get_session_species (): array
    {
        $species = array ();
        if ( isset( $_SESSION[ 'species' ] ) ) {
            $species = unserialize ( $_SESSION[ 'species' ] );
        }
        return $species;
    }

    static public function check_for_one_each ( $species )
    {
        $ownership = array ();
        foreach ( $species as $item ) {
            $ownership[ get_class ( $item ) ] = 1;
        }
        $status = ( count ( $ownership ) == 3 );
        return $status;
    }

    /**
     * @param $species
     * @param $round
     * @return array
     */
    static public function get_game_status ( $species , $round ): array
    {
        /** @var array $status */
        $game_over = true;
        // no human is left
        if ( ! self::check_for_human ( $species ) ) {
            $status = array ( $game_over , '<h2>Game is Over</h2>' );
        }
        else {
            // human exists and there are more rounds
            if ( $round > 0 ) {
                if ( count ( $species ) > 1 ) {
                    $have_cow_and_bunny = self::check_for_one_each ( $species );
                    if ( $have_cow_and_bunny ) {
                        $status = array ( ! $game_over , '<h2>There is still hope</h2>' );
                    }
                    else {
                        $status = array ( $game_over , '<h2>You have lost at least one kind of animals. There is no hope to be a farmer</h2>' );
                    }
                }
                else {
                    // human exists
                    $status = array ( $game_over , '<h2>You are the Only one left. Without any cow or bunny !!!</h2>' );
                }
            }
            else {
                // Human exists and no rounds left
                $have_cow_and_bunny = self::check_for_one_each ( $species );
                if ( $have_cow_and_bunny ) {
                    $status = array ( $game_over , '<h1>We have a WINNER !!!</h1>' );
                }
                else {
                    $status = array ( $game_over , '<h1>Poor ending with too little animals</h1>' );
                }
            }

        }
        if ( $status[ 0 ] ) {
            $status[ 0 ] = 'GameOver';
        }
        else {
            $status[ 0 ] = 'Continue';
        }
        return $status;
    }

    /**
     * reset the game id it is requested
     */
    static public function check_new_game_request ()
    {
        // check if there was a request for new game session
        if ( isset( $_REQUEST[ 'action' ] ) && $_REQUEST[ 'action' ] === 'new' ) {
            // clear session
            session_unset ();
            // reload the page
            header ( 'Location: ' . $_SERVER[ 'PHP_SELF' ] );
            // in case of error stop
            die;
        }
    }


    /**
     * @return bool
     */
    static public function is_new_game (): bool
    {
        return ( ! isset( $_SESSION[ 'species' ] ) );
    }


    /**
     * @return array
     */
    static public function make_new_farm (): array
    {
        $species = array ();
        // add one human
        $species[] = new human();

        // add two cows
        for ( $i = 0 ; $i < 2 ; $i ++ ) {
            $species[] = new cow();
        }

        // add four bunnies
        for ( $i = 0 ; $i < 4 ; $i ++ ) {
            $species[] = new bunny();
        }
        return $species;
    }

}
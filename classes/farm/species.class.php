<?php
/**
 * Created by PhpStorm.
 * User: amfipolis
 * Date: 29/09/2018
 * Time: 00:27
 */

namespace farm;
/**
 * Class species
 * @package farm
 */
class species
{
    static    $counter = 0;
    protected $lifeline;
    protected $duration;
    protected $id;

    /**
     * species constructor.
     * @param int $duration
     *
     * Set the duration of the life line for this class instance
     */
    public function __construct ( int $duration = 8 )
    {
        if ( $duration < 8 ) {
            $duration = 8;
        }
        $this->duration = $duration;
        $this->lifeline = $this->duration;
        $this->id       = ++ self::$counter;
    }

    /**
     * the unlucky class instance is losing one step in its life line duration
     */
    public function decrease ()
    {
        if ( $this->duration > 0 ) {
            $this->duration --;
        }
    }

    /**
     * the lucky instance recovering to full life line level
     */
    public function feed ()
    {
        $this->duration ++; // = $this->lifeline;
    }

    /**
     * @return int
     * get the current duration left for this class instance
     */
    public function get_duration ()
    {
        return $this->duration;
    }

    /**
     * @return int
     * the life span of the class of this instance
     */
    public function get_lifeline ()
    {
        return $this->lifeline;
    }

    /**
     * @return int
     * the id of the class instance
     *
     */
    public function get_id ()
    {
        return $this->id;
    }

    /**
     * @return bool
     * if the instance is dead
     */
    public function is_dead ()
    {
        return ( $this->duration == 0 );
    }

    /**
     * show an image of the class and the id of the instance
     */
    public function show ()
    {
        echo '
<div class="species">' . get_class () . '<br>ID:' . $this->id . ' (' . ( $this->is_dead () ? 'DEAD' : $this->duration ) . ')<br/>
    <img width="50" height="50" class="image" src="./images/' . get_class () . '.png" /></div>';
    }
}
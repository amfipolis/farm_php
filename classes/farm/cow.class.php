<?php
/**
 * Created by PhpStorm.
 * User: amfipolis
 * Date: 29/09/2018
 * Time: 01:32
 */

namespace farm;


/**
 * Class cow
 * @package farm
 */
class cow extends species
{
    public function __construct ( int $duration = 10 )
    {
        parent::__construct ( $duration );
    }

    /**
     * show an image of the class and the id of the instance
     */
    public function show ()
    {
        echo '
<div class="species">' . get_class () . '<br>ID:' . $this->id . ' (' . ( $this->is_dead () ? 'DEAD' : $this->duration ) . ')<br/>
    <img width="50" height="50" class="image" src="./images/' . get_class () . '.png" /></div>';
    }

}
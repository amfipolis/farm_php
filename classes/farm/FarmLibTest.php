<?php
/**
 * Created by PhpStorm.
 * User: amfipolis
 * Date: 29/09/2018
 * Time: 20:45
 */

namespace farm;
// in order to check session related functions we must start a test script with a session declaration
session_start ();
//  must include all related files in order to have access to the test required functions
include_once 'FarmLib.class.php'; // \farm\FarmLib;
include_once 'species.class.php'; // \farm\FarmLib;
include_once 'human.class.php'; // \farm\FarmLib;
include_once 'cow.class.php'; // \farm\FarmLib;
include_once 'bunny.class.php'; // \farm\FarmLib;

use PHPUnit\Framework\TestCase;

class FarmLibTest extends TestCase
{


    /**
     * Test for make new farm
     */
    public function testMake_new_farm ()
    {
        $farm         = \farm\FarmLib::make_new_farm ();
        $animal_count = count ( $farm );
        // 1 human + 2 cows + 4 bunnies = 7 species
        $this->assertEquals ( $animal_count , 7 );
    }

    /**
     * Test for removing dead animals from the farm
     */
    public function testClear_farm ()
    {
        $dead = 0;
        $farm = FarmLib::make_new_farm ();
        FarmLib::clear_farm ( $farm );
        foreach ( $farm as $item ) {
            if ( $item->is_dead () ) {
                $dead ++;
            }
        }
        $this->assertEquals ( $dead , 0 );
    }

    /**
     * Test if there is a human in the farm
     */
    public function testCheck_for_human ()
    {
        $farm = \farm\FarmLib::make_new_farm ();
        $this->assertTrue ( FarmLib::check_for_human ( $farm ) );
    }

    /**
     * Test if there is no side return from the presentation of the farm
     */
    public function testShow_farm ()
    {
        $farm = FarmLib::make_new_farm ();
        $this->assertNull ( FarmLib::show_farm ( $farm ) );
    }

    /**
     * Test if only a human exists in the farm
     */
    public function testCheck_only_human ()
    {
        $farm = FarmLib::make_new_farm ();
        $this->assertFalse ( FarmLib::check_only_human ( $farm ) );
    }

    /**
     * Test if there is a new game request
     */
    public function testCheck_new_game_request ()
    {
        $this->assertNull ( FarmLib::check_new_game_request () );
    }

    /**
     * Test if there is at least one of each species in the farm
     */
    public function testCheck_for_one_each ()
    {
        $farm = FarmLib::make_new_farm ();
        $this->assertTrue ( FarmLib::check_for_one_each ( $farm ) );
    }

    /**
     * Test if the game rounds decrease
     */
    public function testDecrease_session_rounds ()
    {
        // session_start();
        $rounds = FarmLib::get_session_round ();
        FarmLib::decrease_session_rounds ();
        $new_rounds = FarmLib::get_session_round ();
        $this->assertLessThan ( $rounds , $new_rounds );
    }

    /**
     * Test if the farm is stored in the session
     */
    public function testSet_session_species ()
    {
        $farm = FarmLib::make_new_farm ();
        FarmLib::set_session_species ( $farm );
        $this->assertEquals ( $farm , FarmLib::get_session_species () );
    }

    /**
     * Test to see if we receive a status for the game
     */
    public function testGet_game_status ()
    {
        $farm = FarmLib::make_new_farm ();
        FarmLib::set_session_species ( $farm );
        $this->assertNotEmpty ( FarmLib::get_game_status ( $farm , 1 ) );
    }

    /**
     * Test if we read the round of the current game session
     */
    public function testGet_session_round ()
    {
        FarmLib::set_session_round ( 1 );
        $this->assertEquals ( 1 , FarmLib::get_session_round () );
    }

    /**
     * Test if we can read the session related animal list
     */
    public function testGet_session_species ()
    {
        $farm = FarmLib::make_new_farm ();
        FarmLib::set_session_species ( $farm );
        $this->assertEquals ( $farm , FarmLib::get_session_species () );

    }

    public function testIs_new_game ()
    {
        $this->assertFalse ( FarmLib::is_new_game () );
    }

    public function testIs_game_over ()
    {
        $farm  = FarmLib::make_new_farm ();
        $round = 50;
        FarmLib::set_session_round ( $round );
        FarmLib::set_session_species ( $farm );
        $this->assertFalse ( FarmLib::is_game_over ( FarmLib::get_session_species () ) );
    }

    public function testSet_session_round ()
    {
        $round = 1;
        FarmLib::set_session_round ( $round );
        $this->assertEquals ( $round , FarmLib::get_session_round () );
    }

    public function testFeed_farm ()
    {
        $farm = FarmLib::make_new_farm ();
        FarmLib::set_session_species ( $farm );
        $lives_start = 0;
        foreach ( $farm as $animal ) {
            $lives_start += $animal->get_duration ();
        }
        FarmLib::feed_farm ( $farm );
        $farm_later  = FarmLib::get_session_species ();
        $lives_after = 0;
        foreach ( $farm_later as $animal ) {
            $lives_after += $animal->get_duration ();
        }
        $this->assertNotEquals ( $lives_after , $lives_start );
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: amfipolis
 * Date: 29/09/2018
 * Time: 00:19
 */
session_start ();
ini_set ( 'display_errors' , '1' );
define ( 'ROOT' , pathinfo ( __FILE__ , PATHINFO_DIRNAME ) );
error_reporting ( E_ALL );

/**
 * @param $class_name
 */
function load_class ( $class_name )
{
    //path to the class file
    $class_name = str_replace ( '\\' , '/' , $class_name );
    $path       = './classes/' . $class_name . '.class.php';
    if ( file_exists ( $path ) ) {
        /** @noinspection PhpIncludeInspection */
        require_once ( $path );
    }
}

spl_autoload_register ( 'load_class' );

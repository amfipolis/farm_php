<?php
/**
 * Created by PhpStorm.
 * User: amfipolis
 * Date: 29/09/2018
 * Time: 00:51
 */

require_once 'config.php';
echo '<head><title>Farm Game</title></head>';
echo '<link type="text/css"  href="css/farm.css">';
echo '<div class="center-div"><h1>Here is where the Farm begins</h1></div>';

// read the session data and the request fields
\farm\FarmLib::check_new_game_request ();

// check for new game status
$new_game = \farm\FarmLib::is_new_game ();

if ( $new_game ) {
    // make new game structures
    $species = \farm\FarmLib::make_new_farm ();

    // update session
    \farm\FarmLib::set_session_species ( $species );
    \farm\FarmLib::set_session_round ( 50 );
}
else {
    // get session data
    $species = \farm\FarmLib::get_session_species ();
    $round   = \farm\FarmLib::get_session_round ();
    \farm\FarmLib::feed_farm ( $species );
}

// start the content div
echo '<div id="content">';
// read the species array and get the round number
$species = \farm\FarmLib::get_session_species ();
$round   = \farm\FarmLib::get_session_round ();

// game is over when either no rounds left, no human left, or only a human is left
$game_over = \farm\FarmLib::is_game_over ( $species );

// check for game is over
if ( ! $game_over ) {
    echo "<h2>Turns left: $round</h2>";
}
else {
    // reset session
    session_unset ();
}
echo '
<form id="farm_game_feed" target="_self">
';
if ( ! $game_over ) {
    echo '
    <input type="submit" name="action" value="feed">
';
}
echo '
    <input type="submit" name="action" value="new">
</form>
';
\farm\FarmLib::show_farm ( $species );
echo \farm\FarmLib::get_game_status ( $species , $round )[ 1 ];
if ( $new_game ) {
    echo '<h2>New game</h2>';
}
// close content div
echo '</div>';